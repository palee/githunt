package com.gitihunt.details

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gitihunt.TestTransformer
import com.gitihunt.advenceTime
import com.gitihunt.domain.details.GetFollowersCountUseCase
import com.gitihunt.domain.search.LoadWithSortByAscUsersAndRepoUseCase
import com.gitihunt.main.MainViewModel
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DetailsViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: DetailsViewModel
    @Mock
    lateinit var getFollowersCountUseCase: GetFollowersCountUseCase
    val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = DetailsViewModel(getFollowersCountUseCase, TestTransformer(testScheduler))
    }

    @Test
    fun testFollowersCount() {
        val url = "http://test"
        val count = 3
        whenever(getFollowersCountUseCase.execute(url)).thenReturn(Single.just(count))

        viewModel.loadFollowersCount(url)
        testScheduler.advenceTime()

        assertEquals(count, viewModel.followersCount.value)
    }
}