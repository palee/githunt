package com.gitihunt

import com.gitihunt.domain.model.Repository
import com.gitihunt.domain.model.User
import com.gitihunt.network.model.RepositoryResponse
import com.gitihunt.network.model.UserResponse

class FakeData() {
    val idUser = 10
    val login = "johny"
    val avatarUrl = "http://avatar"
    val url = "http://user"
    val followersUrl = "http://folowers"
    val score = 10.44f

    val id = 0
    val name = "John"
    val description = "description"
    val repositoryUrl = "http://repository"
    val repositoryScore = 20.03f

    val userResponse = UserResponse(idUser, login, avatarUrl, url, followersUrl, score)
    val usersResponse = listOf(userResponse)

    val repositoryResponse = RepositoryResponse(name, id, description, userResponse, repositoryUrl, repositoryScore)
    val repositoriesResponse = listOf(repositoryResponse)

    val user = User(idUser, login, avatarUrl, url, followersUrl, score)
    val repository = Repository(name, id, description, user, repositoryUrl, repositoryScore)

    val results = listOf(user, repository)
}