package com.gitihunt.domain.details

import com.gitihunt.FakeData
import com.gitihunt.network.services.FollowersApi
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LoadFollowersUseCaseTest {

    lateinit var loadFollowersUseCase: LoadFollowersUseCase
    @Mock
    lateinit var followersApi: FollowersApi

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        loadFollowersUseCase = LoadFollowersUseCase(followersApi)
    }

    fun execute() {
        val fakeData = FakeData()
        val url = "http://test"
        val followers = listOf(fakeData.user, fakeData.user)
        whenever(followersApi.loadFollowers(url)).thenReturn(Single.just(followers))

        loadFollowersUseCase.execute(url).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(followers)
    }
}