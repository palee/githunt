package com.gitihunt.domain.details

import com.gitihunt.FakeData
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GetFollowersCountUseCaseTest {

    lateinit var getFollowersUseCase: GetFollowersCountUseCase
    @Mock
    lateinit var loadFollowersUseCase: LoadFollowersUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getFollowersUseCase = GetFollowersCountUseCase(loadFollowersUseCase)
    }

    @Test
    fun execute() {
        val fakeData = FakeData()
        val url = "http://test"
        val followers = listOf(fakeData.user, fakeData.user, fakeData.user)
        whenever(loadFollowersUseCase.execute(url)).thenReturn(Single.just(followers))

        getFollowersUseCase.execute(url).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(followers.size)
    }
}