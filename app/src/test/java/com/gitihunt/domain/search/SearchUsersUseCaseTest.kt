package com.gitihunt.domain.search

import com.gitihunt.FakeData
import com.gitihunt.network.model.SearchResponse
import com.gitihunt.network.services.SearchApi
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before

import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SearchUsersUseCaseTest {

    lateinit var searchUsersUseCase: SearchUsersUseCase
    @Mock
    lateinit var searchApi: SearchApi

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        searchUsersUseCase = SearchUsersUseCase(searchApi)
    }

    @Test
    fun loadUsersSuccess() {
        val query = "johny"
        val page = 1
        val usersResponse = FakeData().usersResponse
        val users = listOf(FakeData().user)
        whenever(searchApi.searchForUsers(query, page)).thenReturn(Single.just(SearchResponse(usersResponse)))

        searchUsersUseCase.execute(query, page).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(users)
    }

    @Test
    fun loadUsersFailure() {
        val query = "johny"
        val page = 1
        val throwable = Throwable("error")
        whenever(searchApi.searchForUsers(query, page)).thenReturn(Single.error(throwable))

        searchUsersUseCase.execute(query, page).test()
                .assertComplete()
                .assertValue(emptyList())
    }
}