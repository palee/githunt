package com.gitihunt.domain.search

import com.gitihunt.domain.model.Repository
import com.gitihunt.domain.model.User
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LoadWithSortByAscUsersAndRepoUseCaseTest {

    lateinit var loadWithSortByAscUsersAndRepoUseCase: LoadWithSortByAscUsersAndRepoUseCase
    @Mock
    lateinit var loadUsersAndRepositoriesUseCase: LoadUsersAndRepositoriesUseCase
    @Mock
    lateinit var sortByAscendingResultItemIdUseCase: SortByAscendingResultItemIdUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        loadWithSortByAscUsersAndRepoUseCase = LoadWithSortByAscUsersAndRepoUseCase(
                loadUsersAndRepositoriesUseCase,
                sortByAscendingResultItemIdUseCase
        )
    }

    @Test
    fun loadWithSortByAscUsersAndRepoUseCaseTest() {
        val query = "android"
        val page = 1
        val results = listOf(userWithId(4), repoWithId(2), userWithId(8), repoWithId(1))
        val output = listOf(repoWithId(1), repoWithId(2), userWithId(4), userWithId(8))
        whenever(loadUsersAndRepositoriesUseCase.execute(query, page)).thenReturn(Single.just(results))
        whenever(sortByAscendingResultItemIdUseCase.execute(results)).thenReturn(Single.just(output))

        loadWithSortByAscUsersAndRepoUseCase.execute(query, page).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(output)
    }

    fun userWithId(id: Int) = User(id, "", "", "", "", 0f)

    fun repoWithId(id: Int) = Repository("", id, "", userWithId(0), "", 0f)
}