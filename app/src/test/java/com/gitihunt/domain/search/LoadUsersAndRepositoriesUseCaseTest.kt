package com.gitihunt.domain.search

import com.gitihunt.FakeData
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LoadUsersAndRepositoriesUseCaseTest {

    lateinit var loadUsersAndRepositoriesUseCase: LoadUsersAndRepositoriesUseCase
    @Mock
    lateinit var searchUsersUseCase: SearchUsersUseCase
    @Mock
    lateinit var searchRepositoriesUseCase: SearchRepositoriesUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        loadUsersAndRepositoriesUseCase = LoadUsersAndRepositoriesUseCase(searchUsersUseCase, searchRepositoriesUseCase)
    }

    @Test
    fun loadSuccess() {
        val query = "test"
        val page = 1
        val fakeData = FakeData()
        val combineList = listOf(fakeData.user, fakeData.repository)
        whenever(searchUsersUseCase.execute(query, page)).thenReturn(Single.just(listOf(fakeData.user)))
        whenever(searchRepositoriesUseCase.execute(query, page)).thenReturn(Single.just(listOf(fakeData.repository)))

        loadUsersAndRepositoriesUseCase.execute(query, page).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(combineList)
    }
}