package com.gitihunt.domain.search

import com.gitihunt.FakeData
import com.gitihunt.network.model.SearchResponse
import com.gitihunt.network.services.SearchApi
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before

import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SearchRepositoriesUseCaseTest {

    lateinit var searchRepositoriesUseCase: SearchRepositoriesUseCase
    @Mock
    lateinit var searchApi: SearchApi

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        searchRepositoriesUseCase = SearchRepositoriesUseCase(searchApi)
    }

    @Test
    fun loadRepositoriesSuccess() {
        val query = "android"
        val page = 1
        val repositoriesResponse = FakeData().repositoriesResponse
        val repositories = listOf(FakeData().repository)
        whenever(searchApi.searchForRepositories(query, page)).thenReturn(Single.just(SearchResponse(repositoriesResponse)))

        searchRepositoriesUseCase.execute(query, page).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(repositories)
    }

    @Test
    fun loadRepositoriesFailure() {
        val query = "johny"
        val page = 1
        val throwable = Throwable("error")
        whenever(searchApi.searchForRepositories(query, page)).thenReturn(Single.error(throwable))

        searchRepositoriesUseCase.execute(query, page).test()
                .assertComplete()
                .assertValue(emptyList())
    }
}