package com.gitihunt.domain.search

import com.gitihunt.domain.model.ResultItem
import org.junit.Before

import org.junit.Test
import org.mockito.MockitoAnnotations

class SortByAscendingResultItemIdUseCaseTest {

    lateinit var sortByAscendingResultItemIdUseCase: SortByAscendingResultItemIdUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        sortByAscendingResultItemIdUseCase = SortByAscendingResultItemIdUseCase()
    }

    @Test
    fun sortAscending() {
        val itemWithId5 = ResultItem(5)
        val itemWithId3 = ResultItem(3)
        val itemWithId1 = ResultItem(1)
        val list = listOf(itemWithId5, itemWithId3, itemWithId1)
        val output = listOf(itemWithId1, itemWithId3, itemWithId5)

        sortByAscendingResultItemIdUseCase.execute(list).test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(output)
    }
}