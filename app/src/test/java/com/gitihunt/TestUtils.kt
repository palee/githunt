package com.gitihunt

import io.reactivex.schedulers.TestScheduler
import org.mockito.Mockito
import java.util.concurrent.TimeUnit

fun once() = Mockito.times(1)

fun TestScheduler.advenceTime(time: Long = 1000) {
    advanceTimeBy(time, TimeUnit.MILLISECONDS)
}