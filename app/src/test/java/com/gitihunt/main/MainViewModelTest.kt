package com.gitihunt.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.gitihunt.FakeData
import com.gitihunt.TestTransformer
import com.gitihunt.advenceTime
import com.gitihunt.domain.model.ResultItem
import com.gitihunt.domain.search.LoadWithSortByAscUsersAndRepoUseCase
import com.gitihunt.once
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MainViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: MainViewModel
    @Mock
    lateinit var loadWithSortByAscUsersAndRepoUseCase: LoadWithSortByAscUsersAndRepoUseCase
    val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(loadWithSortByAscUsersAndRepoUseCase, TestTransformer(testScheduler))
    }

    @Test
    fun singleQueryWithStartAnimation() {
        val fakeData = FakeData()
        val query = "android"
        val fakeList = fakeData.results
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query, 1)).thenReturn(Single.just(fakeList))

        viewModel.loadQuery(query)
        testScheduler.advenceTime()

        assertEquals(fakeList, viewModel.searchResults.value)
        assertEquals(true, viewModel.showResultPage.value)
    }

    @Test
    fun doubleQueryWithStartAnimation() {
        val searchObserver = mock<Observer<List<ResultItem>>>()
        val animationStateObserver = mock<Observer<Boolean>>()
        val query = "andr"
        val query2 = "android"
        val fakeList = FakeData().results
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query, 1)).thenReturn(Single.just(fakeList))
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query2, 1)).thenReturn(Single.just(fakeList))
        viewModel.searchResults.observeForever(searchObserver)
        viewModel.showResultPage.observeForever(animationStateObserver)

        viewModel.loadQuery(query)
        testScheduler.advenceTime()
        verify(searchObserver, once()).onChanged(fakeList)
        verify(animationStateObserver, once()).onChanged(true)

        viewModel.loadQuery(query2)
        testScheduler.advenceTime()
        verify(searchObserver, times(2)).onChanged(fakeList)
        verify(animationStateObserver, once()).onChanged(true)
    }

    @Test
    fun doubleQueryWithStartThenClearWithStopAnimation() {
        val fakeData = FakeData()
        val query = "andr"
        val query2 = "android"
        val query3 = ""
        val fakeList = fakeData.results
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query, 1)).thenReturn(Single.just(fakeList))
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query2, 1)).thenReturn(Single.just(fakeList))
        val searchObserver = mock<Observer<List<ResultItem>>>()
        val animationStateObserver = mock<Observer<Boolean>>()
        viewModel.searchResults.observeForever(searchObserver)
        viewModel.showResultPage.observeForever(animationStateObserver)

        viewModel.loadQuery(query)
        testScheduler.advenceTime()
        verify(searchObserver).onChanged(fakeList)
        verify(animationStateObserver, once()).onChanged(true)

        viewModel.loadQuery(query2)
        testScheduler.advenceTime()
        verify(searchObserver, times(2)).onChanged(fakeList)
        verify(animationStateObserver, once()).onChanged(true)

        viewModel.loadQuery(query3)
        testScheduler.advenceTime()
        verify(searchObserver, times(2)).onChanged(fakeList)
        verify(animationStateObserver, once()).onChanged(false)
    }

    @Test
    fun loadSearchError() {
        val query = "android"
        val throwable = Throwable("error")
        whenever(loadWithSortByAscUsersAndRepoUseCase.execute(query, 1)).thenReturn(Single.error(throwable))

        viewModel.loadQuery(query)
        testScheduler.advenceTime()

        assertEquals(throwable, viewModel.error.value)
    }
}