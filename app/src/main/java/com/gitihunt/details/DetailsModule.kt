package com.gitihunt.details

import com.gitihunt.common.rx.RxTransformer
import com.gitihunt.domain.details.GetFollowersCountUseCase
import com.gitihunt.domain.details.LoadFollowersUseCase
import com.gitihunt.network.services.FollowersApi
import dagger.Module
import dagger.Provides

@Module
class DetailsModule {

    @Provides
    fun detailsViewModelFactory(getFollowersCountUseCase: GetFollowersCountUseCase,
                                rxTransformer: RxTransformer): DetailsViewModel.Factory {
        return DetailsViewModel.Factory(getFollowersCountUseCase, rxTransformer)
    }

    @Provides
    fun getFollowersCountUseCase(loadFollowersUseCase: LoadFollowersUseCase) =
            GetFollowersCountUseCase(loadFollowersUseCase)

    @Provides
    fun loadFollowersUseCase(followersApi: FollowersApi) = LoadFollowersUseCase(followersApi)

}