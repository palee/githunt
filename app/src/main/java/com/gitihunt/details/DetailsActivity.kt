package com.gitihunt.details

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gitihunt.R
import com.gitihunt.domain.model.User
import com.gitihunt.utils.observe
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_details.*
import timber.log.Timber
import javax.inject.Inject

class DetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: DetailsViewModel.Factory
    lateinit var viewModel: DetailsViewModel

    companion object {
        private const val AVATAR_URL_EXTRA = "avatar_url"
        const val LOGIN_EXTRA = "login"
        const val FOLLOWERS_URL_EXTRA = "followers_url"

        fun createExtras(user: User) = Bundle().apply {
            putString(AVATAR_URL_EXTRA, user.avatarUrl)
            putString(LOGIN_EXTRA, user.login)
            putString(FOLLOWERS_URL_EXTRA, user.followersUrl)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        viewModel = ViewModelProviders.of(this, factory).get(DetailsViewModel::class.java)

        showUserDetails(intent.getStringExtra(LOGIN_EXTRA), intent.getStringExtra(AVATAR_URL_EXTRA))

        val followersUrl = intent.getStringExtra(FOLLOWERS_URL_EXTRA)
        viewModel.loadFollowersCount(followersUrl)
    }

    override fun onResume() {
        super.onResume()
        viewModel.followersCount.observe(this, ::showFollowersCount)

        viewModel.error.observe(this) {
            Timber.e("error ${it.message}")
        }
    }

    private fun showFollowersCount(number: Int) {
        followersTextView.text = number.toString()
    }

    private fun showUserDetails(login: String, avatarUrl: String) {
        loginTextView.text = login
        Picasso.get().load(avatarUrl)
                .fit()
                .centerCrop()
                .into(avatarImageView)
    }
}
