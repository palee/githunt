package com.gitihunt.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.gitihunt.base.BaseViewModel
import com.gitihunt.common.rx.RxTransformer
import com.gitihunt.domain.details.GetFollowersCountUseCase
import javax.inject.Inject

class DetailsViewModel @Inject constructor(private val getFollowersCountUseCase: GetFollowersCountUseCase,
                                           private val rxTransformer: RxTransformer) : BaseViewModel() {

    val followersCount = MutableLiveData<Int>()
    val error = MutableLiveData<Throwable>()

    fun loadFollowersCount(followersUrl: String) {
        getFollowersCountUseCase.execute(followersUrl)
                .compose(rxTransformer.single())
                .subscribe(followersCount::postValue, error::postValue)
                .remember()
    }

    open class Factory @Inject constructor(private val getFollowersCountUseCase: GetFollowersCountUseCase,
                                           private val rxTransformer: RxTransformer) : ViewModelProvider.Factory {

        open override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailsViewModel(getFollowersCountUseCase, rxTransformer) as T
        }
    }
}