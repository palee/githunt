package com.gitihunt.common.rx

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer

interface RxTransformer {

    fun main(): Scheduler
    fun io(): Scheduler
    fun computation(): Scheduler
    fun <T> single(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(io()).observeOn(main())
    }

    fun <T> observable(): ObservableTransformer<T, T> = ObservableTransformer {
        it.subscribeOn(io()).observeOn(main())
    }

    fun completable(): CompletableTransformer = CompletableTransformer {
        it.subscribeOn(io()).observeOn(main())
    }
}