package com.gitihunt.utils

import android.content.Context
import android.util.DisplayMetrics

fun Context.convertPixelsToDp(px: Float): Float {
    return px / (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}