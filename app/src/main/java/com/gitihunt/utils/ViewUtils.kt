package com.gitihunt.utils

import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.showKeyboard(inputMethodManager: InputMethodManager) {
    requestFocus()
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun View.hideKeyboard(inputMethodManager: InputMethodManager) {
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}