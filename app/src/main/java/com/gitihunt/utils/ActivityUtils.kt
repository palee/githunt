package com.gitihunt.utils

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

inline fun <reified Activity : AppCompatActivity> AppCompatActivity.start() {
    val intent = Intent(this, Activity::class.java)
    startActivity(intent)
}

inline fun <reified Activity : AppCompatActivity> AppCompatActivity.start(extras: Bundle) {
    val intent = Intent(this, Activity::class.java)
    intent.putExtras(extras)
    startActivity(intent)
}
