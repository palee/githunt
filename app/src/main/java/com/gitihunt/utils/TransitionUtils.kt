package com.gitihunt.utils

import android.support.transition.Transition

interface TransitionListener : Transition.TransitionListener {
    override fun onTransitionResume(transition: Transition) {}

    override fun onTransitionPause(transition: Transition) {}

    override fun onTransitionCancel(transition: Transition) {}

    override fun onTransitionStart(transition: Transition) {}

    override fun onTransitionEnd(transition: Transition) {}
}

class TransitionEndListener(val block: (transition: Transition) -> Unit) : TransitionListener {
    override fun onTransitionEnd(transition: Transition) {
        block(transition)
    }
}