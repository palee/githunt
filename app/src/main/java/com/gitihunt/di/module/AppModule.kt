package com.gitihunt.di.module

import android.content.Context
import com.gitihunt.common.rx.IOTransformer
import com.gitihunt.common.rx.RxTransformer
import com.gitihunt.main.MainApplication
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun applicationContext(app: MainApplication): Context {
        return app
    }

    @Provides
    @Singleton
    fun rxTransformer(): RxTransformer {
        return IOTransformer()
    }

    @Provides
    @Singleton
    fun gson(): Gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
}