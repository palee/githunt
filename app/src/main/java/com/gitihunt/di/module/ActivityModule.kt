package com.gitihunt.di.module

import com.gitihunt.details.DetailsActivity
import com.gitihunt.details.DetailsModule
import com.gitihunt.main.MainActivity
import com.gitihunt.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailsModule::class])
    fun bindDetailsActivity(): DetailsActivity
}