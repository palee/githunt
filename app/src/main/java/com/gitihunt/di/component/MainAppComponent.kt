package com.gitihunt.di.component

import com.gitihunt.di.module.ActivityModule
import com.gitihunt.di.module.AppModule
import com.gitihunt.di.module.NetworkModule
import com.gitihunt.main.MainApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    ActivityModule::class,
    AndroidSupportInjectionModule::class
])
interface MainAppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(applicatiion: MainApplication): Builder

        fun build(): MainAppComponent
    }

    fun inject(app: MainApplication)
}