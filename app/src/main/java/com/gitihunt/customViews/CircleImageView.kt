package com.gitihunt.customViews

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.util.AttributeSet
import android.widget.ImageView


class CircleImageView(context: Context, attributeSet: AttributeSet) : ImageView(context, attributeSet) {

    override fun setImageDrawable(drawable: Drawable?) {
        drawable?.let {
            val bitmap = (drawable as BitmapDrawable).bitmap
            val roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, bitmap)
            roundedBitmapDrawable.isCircular = true
            roundedBitmapDrawable.setAntiAlias(true)
            super.setImageDrawable(roundedBitmapDrawable)
        }
    }
}