package com.gitihunt.network.model

data class UserResponse(
        val id: Int,
        val login: String,
        val avatar_url: String,
        val url: String,
        val followers_url: String,
        val score: Float
)