package com.gitihunt.network.model

data class RepositoryResponse(val name: String,
                              val id: Int,
                              val description: String,
                              val owner: UserResponse,
                              val url: String,
                              val score: Float
)
