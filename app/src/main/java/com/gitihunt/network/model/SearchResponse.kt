package com.gitihunt.network.model

data class SearchResponse<T>(val items: List<T>, val total_count: Int = 0)