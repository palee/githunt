package com.gitihunt.network.services

import com.gitihunt.domain.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface FollowersApi {

    @GET
    fun loadFollowers(@Url url: String): Single<List<User>>
}