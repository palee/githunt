package com.gitihunt.network.services

import com.gitihunt.network.model.RepositoryResponse
import com.gitihunt.network.model.SearchResponse
import com.gitihunt.network.model.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("/search/users")
    fun searchForUsers(@Query("q") query: String,
                       @Query("page") page: Int,
                       @Query("order") order: String = "asc"): Single<SearchResponse<UserResponse>>

    @GET("/search/repositories")
    fun searchForRepositories(@Query("q") query: String,
                      @Query("page") page: Int,
                      @Query("order") order: String = "asc"): Single<SearchResponse<RepositoryResponse>>
}