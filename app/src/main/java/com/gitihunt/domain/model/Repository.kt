package com.gitihunt.domain.model

data class Repository(val name: String,
                      val id: Int,
                      val description: String?,
                      val owner: User,
                      val url: String,
                      val score: Float
) : ResultItem(id)