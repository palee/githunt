package com.gitihunt.domain.model

data class User(
        val id: Int,
        val login: String,
        val avatarUrl: String,
        val url: String,
        val followersUrl: String,
        val score: Float
) : ResultItem(id)