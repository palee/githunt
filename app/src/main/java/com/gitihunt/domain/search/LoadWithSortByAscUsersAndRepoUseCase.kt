package com.gitihunt.domain.search

import com.gitihunt.domain.model.ResultItem
import io.reactivex.Single

open class LoadWithSortByAscUsersAndRepoUseCase(
        private val loadUsersAndRepositoriesUseCase: LoadUsersAndRepositoriesUseCase,
        private val sortByAscendingResultItemIdUseCase: SortByAscendingResultItemIdUseCase) {

    open fun execute(query: String, page: Int): Single<List<ResultItem>> {
        return loadUsersAndRepositoriesUseCase.execute(query, page)
                .flatMap(sortByAscendingResultItemIdUseCase::execute)
    }
}