package com.gitihunt.domain.search

import com.gitihunt.domain.model.Repository
import com.gitihunt.domain.model.User
import com.gitihunt.network.model.RepositoryResponse
import com.gitihunt.network.services.SearchApi
import io.reactivex.Observable
import io.reactivex.Single

open class SearchRepositoriesUseCase(private val searchApi: SearchApi) {

    open fun execute(query: String, page: Int): Single<List<Repository>> {
        return searchApi.searchForRepositories(query, page)
                .flatMap {
                    Observable.fromIterable(it.items)
                            .map(::mapper)
                            .toList()
                }.onErrorReturn { emptyList() }
    }

    private fun mapper(repositoryResponse: RepositoryResponse) = repositoryResponse.run {
        Repository(name,
                id,
                description,
                User(owner.id, owner.login, owner.avatar_url, owner.url, owner.followers_url, owner.score),
                url,
                score
        )
    }
}