package com.gitihunt.domain.search

import com.gitihunt.domain.model.ResultItem
import io.reactivex.Observable
import io.reactivex.Single

open class SortByAscendingResultItemIdUseCase {

    open fun execute(items: List<ResultItem>): Single<List<ResultItem>> {
        return Observable.fromIterable(items).toSortedList { resultItem, resultItem2 ->
            resultItem.objectId.compareTo(resultItem2.objectId)
        }
    }
}