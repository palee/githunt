package com.gitihunt.domain.search

import com.gitihunt.domain.model.ResultItem
import io.reactivex.Single
import io.reactivex.functions.BiFunction

open class LoadUsersAndRepositoriesUseCase(private val searchUsersUseCase: SearchUsersUseCase,
                                           private val searchRepositoriesUseCase: SearchRepositoriesUseCase) {

    open fun execute(query: String, page: Int): Single<List<ResultItem>> {
        return searchUsersUseCase.execute(query, page)
                .zipWith(searchRepositoriesUseCase.execute(query, page), BiFunction { users, repositories ->
                    mutableListOf<ResultItem>().apply {
                        addAll(users)
                        addAll(repositories)
                    }
                })
    }
}