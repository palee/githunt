package com.gitihunt.domain.search

import com.gitihunt.domain.model.User
import com.gitihunt.network.model.UserResponse
import com.gitihunt.network.services.SearchApi
import io.reactivex.Observable
import io.reactivex.Single

open class SearchUsersUseCase(private val searchApi: SearchApi) {

    open fun execute(query: String, page: Int): Single<List<User>> {
        return searchApi.searchForUsers(query, page)
                .flatMap {
                    Observable.fromIterable(it.items)
                            .map(::mapper)
                            .toList()
                }.onErrorReturn { emptyList() }
    }

    private fun mapper(userResponse: UserResponse) = userResponse.run {
        User(id, login, avatar_url, url, followers_url, score)
    }

}