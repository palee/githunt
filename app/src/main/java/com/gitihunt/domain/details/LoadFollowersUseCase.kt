package com.gitihunt.domain.details

import com.gitihunt.domain.model.User
import com.gitihunt.network.services.FollowersApi
import io.reactivex.Single

open class LoadFollowersUseCase(private val followersApi: FollowersApi) {
    
    open fun execute(url: String): Single<List<User>> {
        return followersApi.loadFollowers(url)
    }
}