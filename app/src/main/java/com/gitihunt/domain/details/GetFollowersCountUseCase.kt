package com.gitihunt.domain.details

import io.reactivex.Single

open class GetFollowersCountUseCase(private val loadFollowersUseCase: LoadFollowersUseCase) {

    open fun execute(url: String): Single<Int> {
        return loadFollowersUseCase.execute(url)
                .map { it.size }
    }
}