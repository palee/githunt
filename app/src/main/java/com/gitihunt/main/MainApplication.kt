package com.gitihunt.main

import android.app.Activity
import android.app.Application
import com.gitihunt.BuildConfig
import com.gitihunt.di.component.DaggerMainAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class MainApplication: Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerMainAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
}