package com.gitihunt.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.gitihunt.base.BaseViewModel
import com.gitihunt.common.rx.RxTransformer
import com.gitihunt.domain.model.ResultItem
import com.gitihunt.domain.search.LoadWithSortByAscUsersAndRepoUseCase
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
        private val loadWithSortByAscUsersAndRepoUseCase: LoadWithSortByAscUsersAndRepoUseCase,
        private val rxTransformer: RxTransformer
) : BaseViewModel() {

    companion object {
        const val DEBOUNCE_TIMEOUT = 400L
    }

    val searchResults = MutableLiveData<List<ResultItem>>()
    val showResultPage = MutableLiveData<Boolean>()
    val clearResults = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable>()

    private val searchSubject = PublishSubject.create<String>()
    private val loadSubject = PublishSubject.create<String>()
    private var isSearchPage = true
    private var actualPage = 1
    private var actualQuery = ""
    private var allResults = mutableListOf<ResultItem>()

    init {
        searchSubject.doOnNext(::updateScreen)
                .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS, rxTransformer.computation())
                .filter { it.isNotEmpty() }
                .doOnNext { allResults = mutableListOf() }
                .doOnNext { actualPage = 1 }
                .doOnNext { actualQuery = it }
                .subscribe(loadSubject::onNext)
                .remember()

        loadSubject.switchMapSingle(::makeSearchRequest)
                .doOnNext { allResults.addAll(it) }
                .map { allResults }
                .subscribe(searchResults::postValue, error::postValue)
                .remember()
    }

    fun loadQuery(query: String) {
        searchSubject.onNext(query)
    }

    fun refreshQuery() {
        searchSubject.onNext(actualQuery)
    }

    fun loadMore() {
        loadSubject.onNext(actualQuery)
    }

    private fun makeSearchRequest(query: String): Single<List<ResultItem>> {
        return loadWithSortByAscUsersAndRepoUseCase.execute(query, actualPage)
                .compose(rxTransformer.single())
                .doFinally { actualPage++ }
    }

    private fun updateScreen(query: String) {
        if (query.isNotEmpty() && isSearchPage.not()) return
        isSearchPage = query.isEmpty() && isSearchPage.not()
        showResultPage.postValue(isSearchPage.not())
    }

    open class Factory @Inject constructor(private val loadWithSortByAscUsersAndRepoUseCase: LoadWithSortByAscUsersAndRepoUseCase,
                                           private val rxTransformer: RxTransformer) : ViewModelProvider.Factory {

        open override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainViewModel(loadWithSortByAscUsersAndRepoUseCase, rxTransformer) as T
        }
    }
}