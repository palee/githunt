package com.gitihunt.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitihunt.R
import com.gitihunt.R.id.avatarImageView
import com.gitihunt.domain.model.Repository
import com.gitihunt.domain.model.ResultItem
import com.gitihunt.domain.model.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.repository_list_item.view.*
import kotlinx.android.synthetic.main.user_list_item.view.*

class SearchResultsAdapter(val onClickListener: (item: ResultItem) -> Unit) :
        RecyclerView.Adapter<SearchResultsAdapter.SearchResultViewHolder>() {

    companion object {
        const val TYPE_USER = 1
        const val TYPE_REPOSITORY = 2
    }

    var items: List<ResultItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemViewType(position: Int): Int {
        if (items[position] is User)
            return TYPE_USER
        else
            return TYPE_REPOSITORY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val viewHolder = when (viewType) {
            TYPE_USER -> SearchResultViewHolder.UserResultViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false))

            else -> SearchResultViewHolder.RepositoryResultViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.repository_list_item, parent, false))
        }

        viewHolder.itemView.setOnClickListener {
            onClickListener(items[viewHolder.adapterPosition])
        }

        return viewHolder
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        when (holder) {
            is SearchResultViewHolder.UserResultViewHolder -> holder.bind(items[position] as User)
            is SearchResultViewHolder.RepositoryResultViewHolder -> holder.bind(items[position] as Repository)
        }
    }

    fun clear() {
        items = emptyList()
    }

    sealed class SearchResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        class UserResultViewHolder(itemView: View) : SearchResultViewHolder(itemView) {

            fun bind(user: User) = itemView.apply {
                userNameListTextView.text = user.login
                scoreTextView.text = user.score.toString()
                user.avatarUrl.let {
                    Picasso.get()
                            .load(it)
                            .placeholder(R.drawable.user_image_placeholder)
                            .fit()
                            .centerCrop()
                            .into(avatarImageView)
                }
            }
        }

        class RepositoryResultViewHolder(itemView: View) : SearchResultViewHolder(itemView) {

            fun bind(repository: Repository) = itemView.apply {
                titleTextView.text = repository.name
                userNameTextView.text = repository.owner.login
                if(repository.description.isNullOrEmpty()) {
                    descriptionTextView.text = repository.description
                    descriptionTextView.visibility = View.VISIBLE
                }else
                    descriptionTextView.visibility = View.GONE

                scoreRepoTextView.text = repository.score.toString()
                repository.owner.avatarUrl.let {
                    Picasso.get()
                            .load(it)
                            .placeholder(R.drawable.user_image_placeholder)
                            .fit()
                            .centerCrop()
                            .into(avatarRepoImageView)
                }
            }
        }
    }


}