package com.gitihunt.main

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.transition.AutoTransition
import android.support.transition.TransitionManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.gitihunt.R
import com.gitihunt.details.DetailsActivity
import com.gitihunt.domain.model.User
import com.gitihunt.utils.*
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main_empty_page.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: MainViewModel.Factory
    lateinit var viewModel: MainViewModel

    private val constraintSetEmptyPage = ConstraintSet()
    private val constraintSetWithResults = ConstraintSet()

    private val animatorSet = AnimatorSet().apply {
        duration = 200
    }

    private val inputMethodManager by lazy { getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }

    private val linearLayoutManager = LinearLayoutManager(this)
    private val searchResultsAdapter = SearchResultsAdapter {
        if(it is User)
            start<DetailsActivity>(DetailsActivity.createExtras(it))
    }

    private val textWatcher = object : EmptyTextWatcher() {
        override fun afterTextChanged(editable: Editable?) {
            editable?.let {
                showLoadIndicator()
                viewModel.loadQuery(editable.toString())
            }
        }
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            if (swipeRefreshLayout.isRefreshing)
                return
            val visibleItemCount = linearLayoutManager.childCount
            val totalItemCount = linearLayoutManager.itemCount
            val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                showLoadIndicator()
                viewModel.loadMore()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_empty_page)
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)

        setupRecyclerView()
        setupConstraintsAnimation()
        setupSwipeRefreshLayout()
        searchEditText.addTextChangedListener(textWatcher)
        fab.setOnClickListener { selectSearchTextAndShowKeyboard() }
    }

    override fun onResume() {
        super.onResume()

        viewModel.searchResults.observe(this) {
            hideLoadIndicator()
            searchResultsAdapter.items = it
            gitLogoImageView.visibility = View.GONE
        }

        viewModel.clearResults.observe(this) {
            searchResultsAdapter.items = emptyList()
            gitLogoImageView.visibility = View.VISIBLE
        }

        viewModel.showResultPage.observe(this) {
            swipeRefreshLayout.isEnabled = it
            if (it.not()) {
                searchResultsAdapter.clear()
                animateToEmptySearch()
                hideLoadIndicator()
            } else
                animateToResultPage()
        }

        viewModel.error.observe(this) {
            hideLoadIndicator()
            Timber.e("error ${it.message}")
        }
    }


    override fun onBackPressed() {
        if (searchEditText.text.toString().isEmpty())
            super.onBackPressed()
        else
            searchEditText.setText("")
    }

    private fun showLoadIndicator() {
        swipeRefreshLayout.isRefreshing = true
    }

    private fun hideLoadIndicator() {
        swipeRefreshLayout.isRefreshing = false
    }

    private fun animateToResultPage() {
        val scaleTitle: ObjectAnimator = ObjectAnimator.ofFloat(logoTextView, "textSize",
                convertPixelsToDp(logoTextView.textSize), 28f)
        animate(constraintSetWithResults, animatorSet, scaleTitle)
    }

    private fun animateToEmptySearch() {
        val scaleTitle: ObjectAnimator = ObjectAnimator.ofFloat(logoTextView, "textSize",
                convertPixelsToDp(logoTextView.textSize), 54f)
        animate(constraintSetEmptyPage, animatorSet, scaleTitle)
    }

    private fun animate(constraint: ConstraintSet, animatorSet: AnimatorSet, scaleTitle: ObjectAnimator) {
        TransitionManager.beginDelayedTransition(constraintLayout, AutoTransition().apply {
            addListener(TransitionEndListener {
                animatorSet.start()
            })
        })

        constraint.applyTo(constraintLayout)
        animatorSet.play(scaleTitle)
    }

    private fun setupRecyclerView() = recyclerView.apply {
        adapter = searchResultsAdapter
        layoutManager = linearLayoutManager
        addOnScrollListener(scrollListener)
    }

    private fun setupConstraintsAnimation() {
        constraintSetEmptyPage.clone(constraintLayout)
        constraintSetWithResults.clone(this, R.layout.activity_main_with_results)
    }

    private fun setupSwipeRefreshLayout() = swipeRefreshLayout.apply {
        setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent))
        setOnRefreshListener { viewModel.refreshQuery() }
    }

    private fun selectSearchTextAndShowKeyboard() = searchEditText.apply {
        setSelection(0, searchEditText.length())
        showKeyboard(inputMethodManager)
    }

}
