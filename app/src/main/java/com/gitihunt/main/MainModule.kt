package com.gitihunt.main

import com.gitihunt.common.rx.RxTransformer
import com.gitihunt.domain.search.*
import com.gitihunt.network.services.SearchApi
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun mainViewModelFactory(loadWithSortByAscUsersAndRepoUseCase: LoadWithSortByAscUsersAndRepoUseCase,
                             rxTransformer: RxTransformer): MainViewModel.Factory {
        return MainViewModel.Factory(loadWithSortByAscUsersAndRepoUseCase, rxTransformer)
    }

    @Provides
    fun searchUsersUseCase(searchApi: SearchApi) = SearchUsersUseCase(searchApi)

    @Provides
    fun searchRepositoriesUseCase(searchApi: SearchApi) = SearchRepositoriesUseCase(searchApi)

    @Provides
    fun loadUserAndRepositoriesUseCase(searchUsersUseCase: SearchUsersUseCase,
                                       searchRepositoriesUseCase: SearchRepositoriesUseCase) =
            LoadUsersAndRepositoriesUseCase(searchUsersUseCase, searchRepositoriesUseCase)

    @Provides
    fun sortByAscendingResultItemIdUseCase() = SortByAscendingResultItemIdUseCase()

    @Provides
    fun loadWithSortByAscUsersAndRepoUseCase(loadUsersAndRepositoriesUseCase: LoadUsersAndRepositoriesUseCase,
                                             sortByAscendingResultItemIdUseCase: SortByAscendingResultItemIdUseCase) =
            LoadWithSortByAscUsersAndRepoUseCase(loadUsersAndRepositoriesUseCase, sortByAscendingResultItemIdUseCase)

}