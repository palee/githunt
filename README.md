# About
GitHunt - Github users and repositories searching app

# Architecture
MVVM is a pattern which works nicely with Android ViewModel class. In oposite to MVP, ViewModel know nothing about View, so there are less interfaces. 

<img src="https://cdn-images-1.medium.com/max/1600/1*VLhXURHL9rGlxNYe9ydqVg.png" width="736"/>

App is using: 
* Dagger2 for dependency injection
* Retrofit with OKHttp for networking
* RxJava2 for observer pattern and threading
* Timber for logs

# APK
[Download APK from this repository](https://gitlab.com/palee/githunt/blob/master/app/debug/app-debug.apk)
